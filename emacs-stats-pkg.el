(define-package "emacs-stats" "20150000.1145" "Collect statistics about document edition"
  '((cl-lib "0.5"))
  :url "https://github.com/ssaavedra/emacs-stats-el" :keywords
  '("stats" "collaborative edition"))
;; Local Variables:
;; no-byte-compile: t
;; End:
