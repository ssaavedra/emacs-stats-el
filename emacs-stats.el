;;; emacs-stats.el ---Document edit statistics for Emacs-*-lexical-binding:t-*-

;; URL: https://github.com/ssaavedra/emacs-stats.el
;; Package-Requires: ((cl-lib "0.5") (async "1.3"))
;; Keywords: languages, lisp, research, stats
;; Version: 1.0

;;;; License and Commentary

;;     Copyright (C) 2015  Santiago Saavedra López
;;
;;     For a detailed list of contributors, see the manual.
;;
;;     This program is free software; you can redistribute it and/or
;;     modify it under the terms of the GNU General Public License as
;;     published by the Free Software Foundation; either version 2 of
;;     the License, or (at your option) any later version.
;;
;;     This program is distributed in the hope that it will be useful,
;;     but WITHOUT ANY WARRANTY; without even the implied warranty of
;;     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;;     GNU General Public License for more details.
;;
;;     You should have received a copy of the GNU General Public
;;     License along with this program; if not, write to the Free
;;     Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
;;     MA 02111-1307, USA.

;;; Commentary:

;; Emacs-Stats is a statistics collector for Emacs
;;
;; We keep your statistics in a centralized place in order to run some
;; algorithms on them and adequate our next collaborative editing
;; research on our findings about that.

;;; Code:


;;;; Dependencies and setup
(eval-and-compile
  (require 'cl-lib nil t)
  ;; For emacs 23, look for bundled version
  (require 'cl-lib "lib/cl-lib"))

(eval-when-compile (require 'cl)) ; defsetf, lexical-let
(eval-and-compile (require 'async))

(eval-and-compile
  (if (< emacs-major-version 23)
      (error "Emacs-stats requires an Emacs version of 23, or above")))

(require 'request)
(require 'compile)

;; (defgroup emacs-stats "Emacs-Stats features")

(defcustom dstats-filepath (concat user-emacs-directory "_emacs_stats.elx")
  "The path of the file saving the emacs-stats statistics
  locally. Keep in mind that these will be removed as soon as
  they are uploaded.")

(defcustom dstats-max-unsaved-events 100
  "The max number of unsaved events to be held in the
  buffer."
  :group 'emacs-stats)

(defcustom dstats-events-lowwater 10000
  "The min length of the event list to be sent over to the
network before GC."
  :group 'emacs-stats)

(defcustom dstats-delete-events-after-upload t
  "Whether to delete events from the hard drive after they have
  been uploaded successfully to the remote server. If nil, they
  are not deleted. They are not transferred again, although,
  there's a marker for preventing that waste."
  :group 'emacs-stats)

(defcustom dstats-events-hiwater 1000000000
  "The max length of the event list before collecting any further
events."
  :group 'emacs-stats)

(defcustom dstats-url-endpoint "http://127.0.0.1:3000/api/v1/sid/1/stats"
  "The URL where to POST the stats to."
  :group 'emacs-stats)

(defcustom dstats-url-auth-token "Bearer xxxxxx"
  "The Auth Token to authenticate the post data."
  :group 'emacs-stats)

(defvar dstats-current-buffer nil)
(defvar dstats-unsaved-events 0)

;;;###autoload
(define-minor-mode dstats-mode
  "Enable statistics collection for this buffer."
  :lighter " (S)"
  (if dstats-mode (dstats-mode-init (current-buffer))
    (dstats-mode-teardown (current-buffer))))

(define-globalized-minor-mode dstats-global-mode
  dstats-mode
  dstats-mode)

(defmacro dstats-with-events-buffer (&rest body)
  (declare (indent defun))
  `(dstats-with-events-buffer- (lambda () ,@body) nil))

(defun dstats-with-events-buffer- (fn &optional avoid-save-buffer buffer-)
  (let* ((buffer (or buffer- (current-buffer)))
	 (dstats-buffer (find-file-noselect dstats-filepath)))

    ;; Fail fast in this case. This means programming error.
    (when (eq (current-buffer)
	      dstats-buffer)
      (error "Nested dstats-with-events-buffer- call"))
    
    (with-current-buffer dstats-buffer
      ;; If we switched buffers save the switch
      (unless (or avoid-save-buffer
		  (eq buffer dstats-buffer)
		  (eq buffer dstats-current-buffer))
	(dstats--write-buffer-change-to buffer))  

      ;; Then exec the body
      (funcall fn))))

(defun dstats--locate-sentpoint (&optional count)
  (widen)
  (goto-char (point-max))
  (search-backward "(:sendpoint :sent-at" nil "go-to-start-of-file" count))

(defmacro dstats--narrow-to-sentpoint (&rest body)
  `(dstats-with-events-buffer- (lambda ()
				 (save-restriction
				   (save-excursion
				     (when (dstats--locate-sentpoint)
				       (forward-line))
				     (narrow-to-region (point) (point-max))
				     ,@body))) t))


(defun dstats-is-buffer-active (buffer)
  (when buffer
    (dstats--narrow-to-sentpoint
     (goto-char (point-max))
     (cl-labels ((first-part-of (l)
			     (list
			      (car l)
			      (cadr l)
			      (caddr l)
			      (cadddr l)))
		 (bi (action)
		     (substring
		      (prin1-to-string
		       (list action
			     (first-part-of (dstats-buffer-info buffer)))) 0 -10)))
       
       (let ((busw (bi :buffer-switch))
	     (buac (bi :buffer-active)))
	 (let ((busws (save-excursion (search-backward busw nil t)))
	       (buacs (save-excursion (search-backward buac nil t))))
	   (or busws
	       buacs)))))))

;; For the while
;; TODO: Git repo/hash path/etc
(defun dstats-buffer-info (buffer)
  (when buffer
    (list
     :buffer-name (buffer-name buffer)
     :buffer-path-sha256 (secure-hash 'sha256 (file-truename (buffer-file-name buffer)))
     :buffer-initial-size (buffer-size buffer)
     :buffer-projectile-dir (with-current-buffer buffer
			      (let ((projectile-require-project-root nil))
		 		(if (not (eq 'none
					     (projectile-project-vcs)))
				    (projectile-project-root)
				  nil))))))


(defun dstats--save-events-buffer ()
  (when (> dstats-unsaved-events 0)
    (if (not (eq (current-buffer)
		 (find-file-noselect dstats-filepath)))
	(dstats-with-events-buffer- #'dstats--save-events-buffer)

      (insert-before-markers (prin1-to-string (list (time-to-seconds) (list :checkpoint dstats-unsaved-events))))
      (insert-before-markers "\n")
      (goto-char (point-max))
      (save-buffer)
      (setq dstats-unsaved-events 0))))



(defun dstats-insert (obj)
  (goto-char (point-max))
  (insert-before-markers (prin1-to-string (list (time-to-seconds) obj)))
  (insert-before-markers "\n")
  (goto-char (point-max))
  (when (equal dstats-unsaved-events 0)
    (run-with-idle-timer 20 nil #'dstats--save-events-buffer))
  (setq dstats-unsaved-events (1+ dstats-unsaved-events))
  (when (>= dstats-unsaved-events dstats-max-unsaved-events)
    (dstats--save-events-buffer)))


(defun dstats--write-buffer-change-to (new-buffer)
  (dstats-insert (list :buffer-switch (dstats-buffer-info new-buffer) :switch-from (dstats-buffer-info dstats-current-buffer)))
  (setq dstats-current-buffer new-buffer))

(defun dstats--write-buffer-save ()
  (when (or t ;; Debug here
	    (eq dstats-current-buffer
		    (current-buffer)))
    (let ((min (point-min))
	  (max (point-max)))
      (dstats-with-events-buffer
	(dstats-insert (list :buffer-save min max)))
      (run-with-idle-timer 4 nil #'dstats--save-events-buffer))))



(defun dstats-grab-stats (begin end prev-length)
  (unless (dstats-is-buffer-active (current-buffer))
    (dstats--buffer-active (current-buffer)))
  (dstats-with-events-buffer
    ;; (message "Buffer changed")
    (dstats-insert (list :buffer-change begin end prev-length))))


(defun dstats--buffer-active (buffer)
  (dstats-with-events-buffer
    (dstats-insert (list :buffer-active (dstats-buffer-info buffer)))))


(defun dstats--send-to-server-async--deprecated ()
  (async-start
   ;; Child process
   (dstats--send-to-server-wrapper
    (let ((the-content content
		       ;; (with-temp-buffer
		       ;; 	 (insert-file-contents dstats-filepath)
		       ;; 	 (buffer-string))
		       ))
      (request dstats-url-endpoint
	       :sync t
	       :type "PUT"
	       :data (list (cons "stats-buffer" the-content))
	       :headers '(("Auth-Token" . dstats-url-auth-token))
	       :parser 'json-read
	       :success (function*
			 (lambda (&key data &allow-other-keys)
			   (message "I sent: %S" data)))))
    )
   (dstats--send-to-server-result)
   ))


(defun dstats--maybe-kill-exported-events (length-to-delete)
  (when dstats-delete-events-after-upload
    (dstats-with-events-buffer
      (widen)
      (save-excursion
	(when (dstats--locate-sentpoint)
	  (forward-line))
	(kill-region (point-min) (point))))))


(defun dstats--send-to-server ()
  ;; First check whether we need to actually send anything at all
  (dstats-with-events-buffer
    (save-restriction
      (save-excursion
	(when (dstats--locate-sentpoint)
	  (forward-line))
	(when (eq (point) (point-max))
	  (error "No need to send anything yet")))))
  (let ((stats-to-send (dstats--narrow-to-sentpoint
			(buffer-string))))
    (dstats-with-events-buffer
      (dstats-insert `(:sendpoint :sending-to ,dstats-url-endpoint)))
    (request dstats-url-endpoint
	     :type "POST"
	     :data `(("stats" . ,stats-to-send))
	     :headers `(("Authorization" . ,dstats-url-auth-token)
			("Accept" . "application/json")
			)
	     :parser 'json-read
	     :success #'dstats--on-submit-success)))


(defun* dstats--on-submit-success (&key data &allow-other-keys)
    (let ((received-length (assoc-default 'length data)))
      (unless (equal "ok" (assoc-default 'status data))
	(message "Something happened on querying the server")
	(error "Something happened to the server"))

      (dstats-with-events-buffer-
       (lambda ()
	 (dstats-insert `(:sendpoint :sent-at ,(time-to-seconds)))))
      (dstats--maybe-kill-exported-events received-length)))


(defun dstats--send-to-server-result (&rest body)
  (message (prin1-to-string body)))

(defmacro dstats--send-to-server-wrapper (&rest body)
  (with-current-buffer (find-file-noselect dstats-filepath)
    (let ((content (buffer-substring-no-properties (point-min) (point-max))))
      `(funcall (lambda ()
		  (let ((content ,content))
		    ,@body))))))

;; (dstats--send-to-server-wrapper (substring-no-properties content 0 50))


(defun dstats--buffer-inactive (buffer)
  (dstats-with-events-buffer
    (dstats-insert (list :buffer-exit (dstats-buffer-info buffer)))
    (dstats--write-buffer-change-to nil)))

(defun dstats-mode-init (buffer)
  (message "Dstats-mode is now enabled")
  ;; Ensure the buffer is created
  (add-hook 'after-change-functions #'dstats-grab-stats nil t)
  (add-hook 'after-save-hook #'dstats--write-buffer-save nil t)
  (run-with-idle-timer 5 nil #'dstats--save-events-buffer)
  (dstats--buffer-active buffer))


(defun dstats-mode-teardown (buffer)
  (message "Dstats-mode is now disabled")
  (dstats--buffer-inactive buffer)
  (setq after-change-functions (remove  #'dstats-grab-stats after-change-functions))
  (setq after-save-hook (remove  #'dstats--write-buffer-save after-save-hook)))

(provide 'emacs-stats)

;; (require 'emacs-stats (buffer-file-name (current-buffer)))

